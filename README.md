```
brew install android-platform-tools
brew info openjdk
yarn json-server server.json -d 1000 -w
emulator -avd Pixel_4_API_30
emulator -avd Pixel_3a_API_30_x86
yarn install
npx react-native run-android
adb reverse tcp:3000 tcp:3000
adb reverse tcp:8081 tcp:8081
```

https://stackoverflow.com/questions/44009058/even-though-jre-8-is-installed-on-my-mac-no-java-runtime-present-requesting-t
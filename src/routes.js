import React from 'react';
import { Image } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Feed from './pages/Feeds';

import logo from './assets/instagram.png';

const AppNavigator = createStackNavigator({
  Feed: {
    screen: Feed,
  },
}, {
  defaultNavigationOptions: {
    headerTitle: () => <Image source={logo} />,
    headerTitleAlign: 'center',
    headerStyle: {
      backgroundColor: '#f5f5f5'
    }
  }
});

const Routes = createAppContainer(AppNavigator);

export default Routes;